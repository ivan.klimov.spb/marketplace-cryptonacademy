import { task } from "hardhat/config";
import { TASK_MAKE_BID_721 } from "./task-names";

task(TASK_MAKE_BID_721, "Make bid 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .addParam("price", "min Price")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).makeBidERC721(args.tokenid, args.price);

    console.log("Make bid 721 finished");
  });
