import { task } from "hardhat/config";
import { TASK_LIST_ITEM_721 } from "./task-names";

task(TASK_LIST_ITEM_721, "List item 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .addParam("price", "price")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).listItemERC721(args.tokenid, args.price);

    console.log("List item 721 finished");
  });
