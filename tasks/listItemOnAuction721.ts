import { task } from "hardhat/config";
import { TASK_LIST_ITEM_ON_AUCTION_721 } from "./task-names";

task(TASK_LIST_ITEM_ON_AUCTION_721, "List auction 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .addParam("minprice", "min Price")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market
      .connect(account)
      .listItemOnAuctionERC721(args.tokenid, args.minprice);

    console.log("List auction 721 finished");
  });
