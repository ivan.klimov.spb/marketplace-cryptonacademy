import { task } from "hardhat/config";
import { TASK_CANCEL_721 } from "./task-names";

task(TASK_CANCEL_721, "Cancel item 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).cancelERC721(args.tokenid);

    console.log("Cancel item 721 finished");
  });
