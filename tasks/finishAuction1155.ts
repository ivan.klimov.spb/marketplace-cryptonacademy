import { task } from "hardhat/config";
import { TASK_FINISH_AUCTION_1155 } from "./task-names";

task(TASK_FINISH_AUCTION_1155, "Finish auction 1155")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).finishAuctionERC1155(args.tokenid);

    console.log("Finish auction 1155 finished");
  });
