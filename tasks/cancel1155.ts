import { task } from "hardhat/config";
import { TASK_CANCEL_1155 } from "./task-names";

task(TASK_CANCEL_1155, "Cancel item 1155")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).cancelERC1155(args.tokenid);

    console.log("Cancel item 1155 finished");
  });
