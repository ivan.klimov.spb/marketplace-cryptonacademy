import { task } from "hardhat/config";
import { TASK_LIST_ITEM_ON_AUCTION_1155 } from "./task-names";

task(TASK_LIST_ITEM_ON_AUCTION_1155, "List auction 1155")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .addParam("amount", "copies amount")
  .addParam("minprice", "min Price")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market
      .connect(account)
      .listItemOnAuctionERC1155(args.tokenid, args.amount, args.minprice);

    console.log("List auction 1155 finished");
  });
