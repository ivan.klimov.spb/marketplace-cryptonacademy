import { task } from "hardhat/config";
import { TASK_APPROVE_721 } from "./task-names";

task(TASK_APPROVE_721, "Approve token 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("to", "address spender")
  .addParam("tokenid", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let erc721 = await hre.ethers.getContractAt("ERC721IVKLIM", args.contract);

    await erc721.connect(account).approve(args.ro, args.tokenid);

    console.log("Approve token 721 finished");
  });
