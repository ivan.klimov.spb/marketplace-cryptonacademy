import { task } from "hardhat/config";
import { TASK_BUY_ITEM_721 } from "./task-names";

task(TASK_BUY_ITEM_721, "Buy item 721")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).buyItemERC721(args.tokenid);

    console.log("Buy item 721 finished");
  });
