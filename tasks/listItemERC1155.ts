import { task } from "hardhat/config";
import { TASK_LIST_ITEM_1155 } from "./task-names";

task(TASK_LIST_ITEM_1155, "List item 1155")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("tokenid", "token id")
  .addParam("price", "price")
  .addParam("amount", "amount copies")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market
      .connect(account)
      .listItemERC1155(args.tokenid, args.price, args.amount);

    console.log("List item 1155 finished");
  });
