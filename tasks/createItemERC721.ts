import { task } from "hardhat/config";
import { TASK_CREATE_ITEM_721 } from "./task-names";

task(TASK_CREATE_ITEM_721, "Create item 721")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("uri", "uri for token")
  .addParam("owner", "address owner")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market.connect(account).createItemERC721(args.uri, args.owner);

    console.log("Create item 721 finished");
  });
