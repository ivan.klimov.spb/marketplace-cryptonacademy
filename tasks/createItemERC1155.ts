import { task } from "hardhat/config";
import { TASK_CREATE_ITEM_1155 } from "./task-names";

task(TASK_CREATE_ITEM_1155, "Create item 1155")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("uri", "uri for token")
  .addParam("owner", "address owner")
  .addParam("amount", "amount copies")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let market = await hre.ethers.getContractAt("IVKLIMMarket", args.contract);

    await market
      .connect(account)
      .createItemERC1155(args.uri, args.owner, args.amount);

    console.log("Create item 1155 finished");
  });
