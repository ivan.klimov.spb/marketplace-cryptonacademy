import { task } from "hardhat/config";
import { TASK_APPROVE_1155 } from "./task-names";

task(TASK_APPROVE_1155, "Approve token 1155")
  .addParam("contract", "Contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("operator", "address spender")
  .addParam("approved", "token id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let erc1155 = await hre.ethers.getContractAt(
      "ERC1155IVKLIM",
      args.contract
    );

    await erc1155
      .connect(account)
      .setApprovalForAll(args.operator, args.approved);

    console.log("Approve token 1155 finished");
  });
