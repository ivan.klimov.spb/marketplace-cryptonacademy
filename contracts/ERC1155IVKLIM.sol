// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./NFTStorageIVKLIM.sol";

contract ERC1155IVKLIM is ERC1155, NFTStorageIVKLIM, AccessControl {

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    constructor(string memory newURI) ERC1155(newURI){
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        super.setBaseURI(newURI);
    }
     
    function mint(address to,
        uint256 id,
        uint256 amount,
        bytes memory data) external onlyRole(MINTER_ROLE){
          _mint(to, id, amount, data);
     }

     function mintBatch(
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) external onlyRole(MINTER_ROLE)  {
        _mintBatch(to, ids, amounts, data);
    }

    function setTokenURI(uint256 tokenId, string memory newURI) public virtual override onlyRole(MINTER_ROLE) {
        super.setTokenURI(tokenId, newURI);
    }

    function uri(uint256 tokenId) public view virtual override returns (string memory) {
        return getTokenURI(tokenId);
    }

    function setBaseURI(string memory newURI) public virtual override onlyRole(MINTER_ROLE) {
        baseUri = newURI;
        _setURI(newURI);
    }

    function getTokenURI(uint256 tokenId) public virtual override view returns (string memory) {
        string memory currentTokenURI = _tokenURIs[tokenId];
        if(bytes(currentTokenURI).length > 0) {
            return string(abi.encodePacked(currentTokenURI, "{id}"));
        }

        string memory currentBaseURI = baseUri;
        return bytes(currentBaseURI).length > 0 ? string(abi.encodePacked(currentBaseURI, "{id}")) : "";
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override (ERC1155, AccessControl) returns (bool) {
        return super.supportsInterface(interfaceId);
    }
}