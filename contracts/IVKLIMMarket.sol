// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "./ERC721IVKLIM.sol";
import "./ERC1155IVKLIM.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract IVKLIMMarket is AccessControl, ERC721Holder, ERC1155Holder {
    uint256 public auctionTime = 180;

    address public erc20Contract;
    address public erc721Contract;
    address public erc1155Contract;

    uint256 public erc721CollectionLength;
    uint256 public erc1155CollectionLength;

    mapping (uint256 => ListedNFT) _listedERC721;
    mapping (uint256 => ListedNFT) _listedERC1155;

    mapping (uint256 => AuctionListedNFT) _auctionERC721;
    mapping (uint256 => AuctionListedNFT) _auctionERC1155;

    struct ListedNFT {
        address owner;
        uint256 price;
        uint256 amount;
    }

    struct AuctionListedNFT {
        address owner;
        address bidSender;
        uint256 price;
        uint256 bitCount;
        uint256 amount;
        uint256 endTime;
        bool lock;
    }

    constructor (address erc721Address, address erc1155Address, address erc20Address) {
        erc721Contract = erc721Address;
        erc1155Contract = erc1155Address;
        erc20Contract = erc20Address;
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function listedERC721(uint256 tokenId) public view returns(ListedNFT memory) {
        address ownerNFT = _listedERC721[tokenId].owner;
        require(ownerNFT == msg.sender, "IVKLIMMarket: not listed token");
        return _listedERC721[tokenId];
    }

    function listedERC1155(uint256 tokenId) public view returns(ListedNFT memory) {
        address ownerNFT = _listedERC1155[tokenId].owner;
        require(ownerNFT == msg.sender, "IVKLIMMarket: not listed token");
        return _listedERC1155[tokenId];
    }

    function createItemERC721(string memory tokenURI, address owner) external {
        require(owner != address(0), "IVKLIMMarket: can't owner with zero address");

        uint256 currentID = erc721CollectionLength;
        ERC721IVKLIM(erc721Contract).mint(owner, currentID, "");
        ERC721IVKLIM(erc721Contract).setTokenURI(currentID, tokenURI);
        erc721CollectionLength++;
    }

    function createItemERC1155(string memory tokenURI, address owner, uint256 amount) external {
        require(owner != address(0), "IVKLIMMarket: can't owner with zero address");
        require(amount > 0, "IVKLIMMarket: amount can't be less than 1");

        uint256 currentID = erc1155CollectionLength;
        ERC1155IVKLIM(erc1155Contract).mint(owner, currentID, amount, "");
        ERC1155IVKLIM(erc1155Contract).setTokenURI(currentID, tokenURI);
        erc1155CollectionLength++;
    }

    function listItemERC721(uint256 tokenId, uint256 price) external {
        address sender = msg.sender;
        ERC721IVKLIM(erc721Contract).safeTransferFrom(sender, address(this), tokenId);
        _listedERC721[tokenId]= ListedNFT(sender, price, 1);
    }

    function listItemERC1155(uint256 tokenId, uint256 price, uint256 amount) external {
        require(_listedERC1155[tokenId].owner == address(0), "IVKLIMMarket: token with this id is already in the listing");
        address sender = msg.sender;
        ERC1155IVKLIM(erc1155Contract).safeTransferFrom(sender, address(this), tokenId, amount, "");
        _listedERC1155[tokenId]= ListedNFT(sender, price, amount);
    }

    function cancelERC721(uint256 tokenId) external {
        address sender = msg.sender;
        address ownerNFT = _listedERC721[tokenId].owner;
        require(ownerNFT == sender, "IVKLIMMarket: not your listed token");

        _listedERC721[tokenId]= ListedNFT(address(0), type(uint256).max, 0);
        ERC721IVKLIM(erc721Contract).safeTransferFrom(address(this), sender, tokenId);
    }

    function cancelERC1155(uint256 tokenId) external {
        address sender = msg.sender;
        address ownerNFT = _listedERC1155[tokenId].owner;
        require(ownerNFT == sender, "IVKLIMMarket: not your listed token");
        uint256 amount = _listedERC1155[tokenId].amount;

        _listedERC1155[tokenId]= ListedNFT(address(0), type(uint256).max, 0);
        ERC1155IVKLIM(erc1155Contract).safeTransferFrom(address(this), sender, tokenId, amount, "");
    }

    function buyItemERC721(uint256 tokenId) external {
        address sender = msg.sender;
        address ownerNFT = _listedERC721[tokenId].owner;
        uint256 priceNFT = _listedERC721[tokenId].price;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        require(IERC20(erc20Contract).allowance(sender, address(this)) >= priceNFT,
         "IVKLIMMarket: not enough approved tokens");
        require(IERC20(erc20Contract).balanceOf(sender) >= priceNFT,
         "IVKLIMMarket: not enough amount tokens");

        _listedERC721[tokenId]= ListedNFT(address(0), type(uint256).max, 1);
        
        IERC20(erc20Contract).transferFrom(sender, ownerNFT, priceNFT);
        ERC721IVKLIM(erc721Contract).safeTransferFrom(address(this), sender, tokenId);
    }

    function buyItemERC1155(uint256 tokenId) external {
        address sender = msg.sender;
        address ownerNFT = _listedERC1155[tokenId].owner;
        uint256 priceNFT = _listedERC1155[tokenId].price;
        uint256 amount = _listedERC1155[tokenId].amount;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        require(IERC20(erc20Contract).allowance(sender, address(this)) >= priceNFT,
         "IVKLIMMarket: not enough approved tokens");
        require(IERC20(erc20Contract).balanceOf(sender) >= priceNFT,
         "IVKLIMMarket: not enough amount tokens");

        _listedERC1155[tokenId]= ListedNFT(address(0), type(uint256).max, 0);
        
        IERC20(erc20Contract).transferFrom(sender, ownerNFT, priceNFT);
        ERC1155IVKLIM(erc1155Contract).safeTransferFrom(address(this), sender, tokenId, amount, "");
    }

    function listItemOnAuctionERC721(uint256 tokenId, uint256 minPrice) external {
        address sender = msg.sender;
        ERC721IVKLIM(erc721Contract).safeTransferFrom(sender, address(this), tokenId);

        _auctionERC721[tokenId].endTime = (block.timestamp + auctionTime); 
        _auctionERC721[tokenId].owner = sender;
        _auctionERC721[tokenId].price = minPrice;
    }

    function listItemOnAuctionERC1155(uint256 tokenId, uint256 amount, uint256 minPrice) external {
        require(_auctionERC1155[tokenId].owner == address(0), "IVKLIMMarket: token with this id is already in the listing auction");
        address sender = msg.sender;
        ERC1155IVKLIM(erc1155Contract).safeTransferFrom(sender, address(this), tokenId, amount, "");

        _auctionERC1155[tokenId].endTime = (block.timestamp + auctionTime); 
        _auctionERC1155[tokenId].owner = sender;
        _auctionERC1155[tokenId].price = minPrice;
        _auctionERC1155[tokenId].amount = amount;
    }

    function makeBidERC721(uint256 tokenId, uint256 price) external {
        address sender = msg.sender;
        address ownerNFT = _auctionERC721[tokenId].owner;
        uint256 priceNFT = _auctionERC721[tokenId].price;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        
        require(IERC20(erc20Contract).allowance(sender, address(this)) >= priceNFT,
         "IVKLIMMarket: not enough approved tokens");
        require(IERC20(erc20Contract).balanceOf(sender) >= priceNFT,
         "IVKLIMMarket: not enough amount tokens");
        require(price > priceNFT,
         "IVKLIMMarket: price must be more than previous");
        require( _auctionERC721[tokenId].endTime > block.timestamp,
         "IVKLIMMarket: auction time has ended");

        address prevBidSender = _auctionERC721[tokenId].bidSender;
        uint256 prevPrice = priceNFT;
        
        _auctionERC721[tokenId].price = type(uint256).max;

        IERC20(erc20Contract).transferFrom(sender, address(this), price);

        if(prevBidSender != address(0)) {
            IERC20(erc20Contract).transfer(prevBidSender, prevPrice);
        }

        _auctionERC721[tokenId].bitCount++;
        
        _auctionERC721[tokenId].bidSender = sender;
        _auctionERC721[tokenId].price = price;
    }

    function makeBidERC1155(uint256 tokenId, uint256 price) external {
        address sender = msg.sender;
        address ownerNFT = _auctionERC1155[tokenId].owner;
        uint256 priceNFT = _auctionERC1155[tokenId].price;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        require(IERC20(erc20Contract).allowance(sender, address(this)) >= priceNFT,
         "IVKLIMMarket: not enough approved tokens");
        require(IERC20(erc20Contract).balanceOf(sender) >= priceNFT,
         "IVKLIMMarket: not enough amount tokens");
        require(price > priceNFT,
         "IVKLIMMarket: price must be more than previous");
        require( _auctionERC1155[tokenId].endTime > block.timestamp,
         "IVKLIMMarket: auction time has ended");

        address prevBidSender = _auctionERC1155[tokenId].bidSender;
        uint256 prevPrice = priceNFT;
        
        _auctionERC1155[tokenId].price = type(uint256).max;

        IERC20(erc20Contract).transferFrom(sender, address(this), price);

        if(prevBidSender != address(0)) {
            IERC20(erc20Contract).transfer(prevBidSender, prevPrice);
        }

        _auctionERC1155[tokenId].bitCount++;
        _auctionERC1155[tokenId].bidSender = sender;
        _auctionERC1155[tokenId].price = price;
    }

    function finishAuctionERC721(uint256 tokenId) external {
        address ownerNFT = _auctionERC721[tokenId].owner;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        require( _auctionERC721[tokenId].endTime <= block.timestamp,
         "IVKLIMMarket: auction can be finished before end of time");

    //    require( !_auctionERC721[tokenId].lock,"IVKLIMMarket: auction is ending");

    //    _auctionERC721[tokenId].lock = true;

        _auctionERC721[tokenId].owner = address(0);

        if(_auctionERC721[tokenId].bitCount <= 2) {
            ERC721IVKLIM(erc721Contract).safeTransferFrom(address(this), ownerNFT, tokenId);
            if(_auctionERC721[tokenId].bidSender != address(0)) {
                IERC20(erc20Contract).transferFrom(address(this), _auctionERC721[tokenId].bidSender, _auctionERC721[tokenId].price);
            }
        } else {
            //if bid account can't receive nft token
            //TODO: ask how to test it
            //try 
            ERC721IVKLIM(erc721Contract).safeTransferFrom(address(this), _auctionERC721[tokenId].bidSender, tokenId); 
            //{
            IERC20(erc20Contract).transferFrom(address(this),ownerNFT, _auctionERC721[tokenId].price);
//            } catch (bytes memory reason) {
//                ERC721IVKLIM(erc721Contract).safeTransferFrom(address(this), ownerNFT, tokenId);
//                IERC20(erc20Contract).transferFrom(address(this), _auctionERC721[tokenId].bidSender, _auctionERC721[tokenId].price);
//            }
        }
        _auctionERC721[tokenId].bidSender = address(0);
        _auctionERC721[tokenId].price = type(uint256).max;
        _auctionERC721[tokenId].bitCount = 0;

 //       _auctionERC721[tokenId].lock = false;
    }

    function finishAuctionERC1155(uint256 tokenId) external {
        address ownerNFT = _auctionERC1155[tokenId].owner;
        require(ownerNFT != address(0), "IVKLIMMarket: current tokenId not listed");
        require( _auctionERC1155[tokenId].endTime <= block.timestamp,
         "IVKLIMMarket: auction can be finished before end of time");

        //require( !_auctionERC1155[tokenId].lock, "IVKLIMMarket: auction is ending");

        //_auctionERC1155[tokenId].lock = true;
        _auctionERC1155[tokenId].owner = address(0);
        uint256 amount = _auctionERC1155[tokenId].amount;

        if(_auctionERC1155[tokenId].bitCount <= 2) {
            ERC1155IVKLIM(erc1155Contract).safeTransferFrom(address(this), ownerNFT, tokenId, amount, "");
            if(_auctionERC1155[tokenId].bidSender != address(0)) {
                IERC20(erc20Contract).transferFrom(address(this), _auctionERC1155[tokenId].bidSender, _auctionERC1155[tokenId].price);
            }
        } else {
            //if bid account can't receive nft token
            //TODO: ask how to test it
            //try 
            ERC1155IVKLIM(erc1155Contract).safeTransferFrom(address(this), _auctionERC1155[tokenId].bidSender, tokenId, amount, "");
            // {
            IERC20(erc20Contract).transferFrom(address(this), ownerNFT, _auctionERC1155[tokenId].price);
   /*         } catch (bytes memory reason) {
                ERC1155IVKLIM(erc1155Contract).safeTransferFrom(address(this), ownerNFT, tokenId, amount, "");
                IERC20(erc20Contract).transferFrom(address(this), _auctionERC1155[tokenId].bidSender, _auctionERC1155[tokenId].price);
            }*/
        }

        _auctionERC1155[tokenId].owner = address(0);
        _auctionERC1155[tokenId].bidSender = address(0);
        _auctionERC1155[tokenId].price = type(uint256).max;
        _auctionERC1155[tokenId].bitCount = 0;
        _auctionERC1155[tokenId].amount = 0;

       // _auctionERC1155[tokenId].lock = false;
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override (AccessControl, ERC1155Receiver) returns (bool) {
          return super.supportsInterface(interfaceId);
    }
}