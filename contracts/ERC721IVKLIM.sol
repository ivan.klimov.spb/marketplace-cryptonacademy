// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./NFTStorageIVKLIM.sol";
import "./IERC721Mintable.sol";

contract ERC721IVKLIM is ERC721, NFTStorageIVKLIM, AccessControl, IERC721Mintable {
     
     bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

     constructor(string memory name, string memory symbol) ERC721(name, symbol){
          _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
     }

     function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
          require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
          return getTokenURI(tokenId);
     }

     function _baseURI() internal view virtual override returns (string memory) {
          return baseUri;
     }

     function getBaseURI() external view returns (string memory) {
          return _baseURI();
     }

     function setBaseURI(string memory newURI) public virtual override onlyRole(MINTER_ROLE) {
          super.setBaseURI(newURI);
     }

     function setTokenURI(uint256 tokenId, string memory newURI) public virtual override onlyRole(MINTER_ROLE) {
          require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
          super.setTokenURI(tokenId, newURI);
     }
     
     function mint(address to,
          uint256 tokenId,
          bytes memory _data) external override onlyRole(MINTER_ROLE) {
          _safeMint(to, tokenId, _data);
     }

     function supportsInterface(bytes4 interfaceId) public view virtual override (ERC721, AccessControl) returns (bool) {
          return super.supportsInterface(interfaceId);
     }
}