// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

interface IERC721Mintable {
    function mint(address to,
        uint256 tokenId,
        bytes memory _data) external;
}