import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

const { ERC721, ERC1155, ERC20 } = process.env;

import { ethers } from "hardhat";
import { IVKLIMMarket } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("IVKLIMMarket");
  console.log("Deploying IVKLIMMarket...");

  const contract: IVKLIMMarket = await factory.deploy(
    ERC721 as string,
    ERC1155 as string,
    ERC20 as string
  );

  await contract.deployed();

  console.log("IVKLIMMarket deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
