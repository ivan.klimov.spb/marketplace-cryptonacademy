import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC721IVKLIM } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("ERC721IVKLIM");
  console.log("Deploying ERC721IVKLIM...");

  const contract: ERC721IVKLIM = await factory.deploy("IVKLIMNFT", "IVP");

  await contract.deployed();

  console.log("ERC721IVKLIM deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
