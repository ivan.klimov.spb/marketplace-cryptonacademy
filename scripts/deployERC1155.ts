import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC1155IVKLIM } from "../src/types";

const { BASE_URI } = process.env;

async function main() {
  const factory = await ethers.getContractFactory("ERC1155IVKLIM");
  console.log("Deploying ERC1155IVKLIM...");

  const contract: ERC1155IVKLIM = await factory.deploy(BASE_URI as string);

  await contract.deployed();

  console.log("ERC1155IVKLIM deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
