/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { Contract, Signer, utils } from "ethers";
import type { Provider } from "@ethersproject/providers";
import type {
  IERC721Mintable,
  IERC721MintableInterface,
} from "../../contracts/IERC721Mintable";

const _abi = [
  {
    inputs: [
      {
        internalType: "address",
        name: "to",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "tokenId",
        type: "uint256",
      },
      {
        internalType: "bytes",
        name: "_data",
        type: "bytes",
      },
    ],
    name: "mint",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
];

export class IERC721Mintable__factory {
  static readonly abi = _abi;
  static createInterface(): IERC721MintableInterface {
    return new utils.Interface(_abi) as IERC721MintableInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): IERC721Mintable {
    return new Contract(address, _abi, signerOrProvider) as IERC721Mintable;
  }
}
