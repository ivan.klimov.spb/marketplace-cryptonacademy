import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;
const TOKEN_ID = 0;

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

    //listing
    await erc721Token.approve(market.address, TOKEN_ID);
    await market.listItemERC721(TOKEN_ID, PRICE);

    await erc1155Token.setApprovalForAll(market.address, true);
    await market.listItemERC1155(TOKEN_ID, PRICE, TOKEN_AMOUNT);
  });

  describe("Cancel item", function () {
    it("after cancel tokenERC721 and nft will be back to owner", async function () {
      const tokenId = TOKEN_ID;
      const prevOwner = (await market.listedERC721(tokenId)).owner;
      await market.cancelERC721(tokenId);
      expect(await erc721Token.ownerOf(tokenId)).to.equal(prevOwner);
    });

    it("after cancel tokenERC1155 and nft will be back to owner", async function () {
      const tokenId = TOKEN_ID;
      const prevOwner = (await market.listedERC1155(tokenId)).owner;
      await market.cancelERC1155(tokenId);
      expect(await erc1155Token.balanceOf(prevOwner, tokenId)).to.equal(
        TOKEN_AMOUNT
      );
    });

    describe("Reverted", function () {
      it("cancel ERC721  not your listed token", async () => {
        await expect(
          market.connect(accounts[1]).cancelERC721(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not your listed token");
      });

      it("cancel ERC1155  not your listed token", async () => {
        await expect(
          market.connect(accounts[1]).cancelERC1155(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not your listed token");
      });
    });
  });
});
