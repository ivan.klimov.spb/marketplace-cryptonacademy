import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
  AccessControl,
} from "../../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;
const TOKEN_ID = 0;
const MINTED_AMOUNT = 1000;

const MINTER_ROLE: string = "MINTER_ROLE";

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

    //mint tokens erc20
    await erc20Token.grantRole(
      ethers.utils.id(MINTER_ROLE),
      accounts[0].address
    );
    await erc20Token.mint(accounts[0].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[1].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[2].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[3].address, MINTED_AMOUNT);
    //approve
    await erc20Token
      .connect(accounts[0])
      .approve(market.address, MINTED_AMOUNT);
    await erc20Token
      .connect(accounts[1])
      .approve(market.address, MINTED_AMOUNT);
    await erc20Token
      .connect(accounts[2])
      .approve(market.address, MINTED_AMOUNT);
    await erc20Token
      .connect(accounts[3])
      .approve(market.address, MINTED_AMOUNT);
    //approve transfer from account[0] to market
    await erc721Token.approve(market.address, TOKEN_ID);
    await erc1155Token.setApprovalForAll(market.address, true);

    //listItemOnAuction
    await market.listItemOnAuctionERC721(TOKEN_ID, PRICE);
    await market.listItemOnAuctionERC1155(TOKEN_ID, TOKEN_AMOUNT, PRICE);
  });

  describe("Finish Auction ", function () {
    it("finishAuctionERC721 nft will back to owner less than 2 bid", async function () {
      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC721(TOKEN_ID);

      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(accounts[0].address);
    });

    it("finishAuctionERC721 nft will back to owner less than 2 bid", async function () {
      const bid = PRICE * 2;
      await market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid);

      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC721(TOKEN_ID);

      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(accounts[0].address);
    });

    it("finishAuctionERC721 nft will send to winner", async function () {
      const bid = PRICE * 2;
      await market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid);

      const bid3 = PRICE * 3;
      await market.connect(accounts[2]).makeBidERC721(TOKEN_ID, bid3);

      const bid4 = PRICE * 4;
      await market.connect(accounts[3]).makeBidERC721(TOKEN_ID, bid4);

      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC721(TOKEN_ID);

      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(accounts[3].address);
    });

    //ERC1155
    it("finishAuctionERC1155 nft will back to owner less than 2 bid", async function () {
      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC1155(TOKEN_ID);

      expect(
        await erc1155Token.balanceOf(accounts[0].address, TOKEN_ID)
      ).to.equal(TOKEN_AMOUNT);
    });

    it("finishAuctionERC1155 nft will back to owner less than 2 bid", async function () {
      const bid = PRICE * 2;
      await market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid);

      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC1155(TOKEN_ID);

      expect(
        await erc1155Token.balanceOf(accounts[0].address, TOKEN_ID)
      ).to.equal(TOKEN_AMOUNT);
    });

    it("finishAuctionERC1155 nft will send to winner", async function () {
      const bid = PRICE * 2;
      await market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid);

      const bid3 = PRICE * 3;
      await market.connect(accounts[2]).makeBidERC1155(TOKEN_ID, bid3);

      const bid4 = PRICE * 4;
      await market.connect(accounts[3]).makeBidERC1155(TOKEN_ID, bid4);

      const timeStamp = await getTimestamp();
      const auctionTime = await market.auctionTime();
      await ethers.provider.send("evm_mine", [
        timeStamp + auctionTime.toNumber(),
      ]);

      await market.finishAuctionERC1155(TOKEN_ID);

      expect(
        await erc1155Token.balanceOf(accounts[3].address, TOKEN_ID)
      ).to.equal(TOKEN_AMOUNT);
    });

    describe("Reverted", function () {
      //ERC721
      it("finishAuctionERC721 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).finishAuctionERC721(TOKEN_ID + 1)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("finishAuctionERC721 auction can be finished before end of time", async () => {
        await expect(
          market.connect(accounts[1]).finishAuctionERC721(TOKEN_ID)
        ).to.be.revertedWith(
          "IVKLIMMarket: auction can be finished before end of time"
        );
      });
      //ERC1155
      it("finishAuctionERC1155 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).finishAuctionERC1155(TOKEN_ID + 1)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("finishAuctionERC1155 auction can be finished before end of time", async () => {
        await expect(
          market.connect(accounts[1]).finishAuctionERC1155(TOKEN_ID)
        ).to.be.revertedWith(
          "IVKLIMMarket: auction can be finished before end of time"
        );
      });
    });
  });
  async function getTimestamp() {
    const blockNumber = await ethers.provider.getBlockNumber();
    const block = await ethers.provider.getBlock(blockNumber);
    return block.timestamp;
  }
});
