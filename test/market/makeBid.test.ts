import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
  AccessControl,
} from "../../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;
const TOKEN_ID = 0;
const MINTED_AMOUNT = 1000;

const MINTER_ROLE: string = "MINTER_ROLE";

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

    //mint tokens erc20
    await erc20Token.grantRole(
      ethers.utils.id(MINTER_ROLE),
      accounts[0].address
    );
    await erc20Token.mint(accounts[0].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[1].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[2].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[3].address, MINTED_AMOUNT);

    //approve transfer from account[0] to market
    await erc721Token.approve(market.address, TOKEN_ID);
    await erc1155Token.setApprovalForAll(market.address, true);

    //listItemOnAuction
    await market.listItemOnAuctionERC721(TOKEN_ID, PRICE);
    await market.listItemOnAuctionERC1155(TOKEN_ID, TOKEN_AMOUNT, PRICE);
  });

  describe("Make Bid", function () {
    it("makeBidERC721 bidder sent amount for nft", async function () {
      const amountERC20Before = await erc20Token.balanceOf(accounts[1].address);
      const bid = PRICE * 2;
      await erc20Token.connect(accounts[1]).approve(market.address, bid);
      await market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid);
      const amountERC20After = await erc20Token.balanceOf(accounts[1].address);
      const subERC20 = (await amountERC20After)
        .sub(amountERC20Before)
        .toNumber();
      expect(-subERC20).to.equal(bid);
    });

    it("makeBidERC721 next bidder sent amount for nft and market send back token to prev account", async function () {
      const bid = PRICE * 2;
      await erc20Token.connect(accounts[1]).approve(market.address, bid);
      await market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid);
      const amountERC20BeforeNextBid = await erc20Token.balanceOf(
        accounts[1].address
      );

      const bid2 = PRICE * 3;
      await erc20Token.connect(accounts[2]).approve(market.address, bid2);
      await market.connect(accounts[2]).makeBidERC721(TOKEN_ID, bid2);

      const amountERC20AfterNextBid = await erc20Token.balanceOf(
        accounts[1].address
      );
      const subERC20 = (await amountERC20AfterNextBid)
        .sub(amountERC20BeforeNextBid)
        .toNumber();
      expect(subERC20).to.equal(bid);
    });

    it("makeBidERC1155 bidder sent amount for nft", async function () {
      const amountERC20Before = await erc20Token.balanceOf(accounts[1].address);
      const bid = PRICE * 2;
      await erc20Token.connect(accounts[1]).approve(market.address, bid);
      await market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid);
      const amountERC20After = await erc20Token.balanceOf(accounts[1].address);
      const subERC20 = (await amountERC20After)
        .sub(amountERC20Before)
        .toNumber();
      expect(-subERC20).to.equal(bid);
    });

    it("makeBidERC1155 next bidder sent amount for nft and market send back token to prev account", async function () {
      const bid = PRICE * 2;
      await erc20Token.connect(accounts[1]).approve(market.address, bid);
      await market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid);
      const amountERC20BeforeNextBid = await erc20Token.balanceOf(
        accounts[1].address
      );

      const bid2 = PRICE * 3;
      await erc20Token.connect(accounts[2]).approve(market.address, bid2);
      await market.connect(accounts[2]).makeBidERC1155(TOKEN_ID, bid2);

      const amountERC20AfterNextBid = await erc20Token.balanceOf(
        accounts[1].address
      );
      const subERC20 = (await amountERC20AfterNextBid)
        .sub(amountERC20BeforeNextBid)
        .toNumber();
      expect(subERC20).to.equal(bid);
    });

    describe("Reverted", function () {
      //ERC721
      it("makeBidERC721 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).makeBidERC721(TOKEN_ID + 1, PRICE)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("makeBidERC721 not enough approved tokens", async () => {
        await expect(
          market.connect(accounts[1]).makeBidERC721(TOKEN_ID, PRICE)
        ).to.be.revertedWith("IVKLIMMarket: not enough approved tokens");
      });

      it("makeBidERC721 not enough amount tokens", async () => {
        const bid = MINTED_AMOUNT * 2;
        await erc20Token.connect(accounts[5]).approve(market.address, bid);
        await expect(
          market.connect(accounts[5]).makeBidERC721(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: not enough amount tokens");
      });

      it("makeBidERC721 price must be more than previous", async () => {
        const bid = 1;
        await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
        await expect(
          market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: price must be more than previous");
      });

      it("makeBidERC721 auction time has ended", async () => {
        const bid = PRICE * 2;
        await erc20Token.connect(accounts[1]).approve(market.address, bid);

        const timeStamp = await getTimestamp();
        const auctionTime = await market.auctionTime();
        await ethers.provider.send("evm_mine", [
          timeStamp + auctionTime.toNumber(),
        ]);

        await expect(
          market.connect(accounts[1]).makeBidERC721(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: auction time has ended");
      });

      //ERC1155
      it("makeBidERC1155 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).makeBidERC1155(TOKEN_ID + 1, PRICE)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("makeBidERC1155 not enough approved tokens", async () => {
        await expect(
          market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, PRICE)
        ).to.be.revertedWith("IVKLIMMarket: not enough approved tokens");
      });

      it("makeBidERC1155 not enough amount tokens", async () => {
        const bid = MINTED_AMOUNT * 2;
        await erc20Token.connect(accounts[5]).approve(market.address, bid);
        await expect(
          market.connect(accounts[5]).makeBidERC1155(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: not enough amount tokens");
      });

      it("makeBidERC1155 price must be more than previous", async () => {
        const bid = 1;
        await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
        await expect(
          market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: price must be more than previous");
      });

      it("makeBidERC1155 auction time has ended", async () => {
        const bid = PRICE * 2;
        await erc20Token.connect(accounts[1]).approve(market.address, bid);

        const timeStamp = await getTimestamp();
        const auctionTime = await market.auctionTime();
        await ethers.provider.send("evm_mine", [
          timeStamp + auctionTime.toNumber(),
        ]);

        await expect(
          market.connect(accounts[1]).makeBidERC1155(TOKEN_ID, bid)
        ).to.be.revertedWith("IVKLIMMarket: auction time has ended");
      });
    });
  });
  async function getTimestamp() {
    const blockNumber = await ethers.provider.getBlockNumber();
    const block = await ethers.provider.getBlock(blockNumber);
    return block.timestamp;
  }
});
