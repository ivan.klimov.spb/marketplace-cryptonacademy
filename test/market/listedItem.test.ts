import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);
  });

  describe("List item", function () {
    it("list token and market will be owner erc721", async function () {
      const tokenId = 0;
      await erc721Token.approve(market.address, tokenId);
      await market.listItemERC721(tokenId, PRICE);
      expect(await erc721Token.ownerOf(tokenId)).to.equal(market.address);
    });

    it("listed erc721 has correct owner", async function () {
      const tokenId = 0;
      await erc721Token.approve(market.address, tokenId);
      await market.listItemERC721(tokenId, PRICE);
      const ListedNFT = await market.listedERC721(tokenId);
      expect(ListedNFT.owner).to.equal(accounts[0].address);
    });

    it("listed erc721 has correct price", async function () {
      const tokenId = 0;
      await erc721Token.approve(market.address, tokenId);
      await market.listItemERC721(tokenId, PRICE);
      const ListedNFT = await market.listedERC721(tokenId);
      expect(ListedNFT.price).to.equal(PRICE);
    });

    it("list token and market will be owner erc1155", async function () {
      const tokenId = 0;
      expect(await erc1155Token.balanceOf(market.address, tokenId)).to.equal(0);
      await erc1155Token.setApprovalForAll(market.address, true);
      await market.listItemERC1155(tokenId, PRICE, TOKEN_AMOUNT);
      expect(await erc1155Token.balanceOf(market.address, tokenId)).to.equal(
        TOKEN_AMOUNT
      );
    });

    it("listed erc1155 has correct owner", async function () {
      const tokenId = 0;
      await erc1155Token.setApprovalForAll(market.address, true);
      await market.listItemERC1155(tokenId, PRICE, TOKEN_AMOUNT);
      const ListedNFT = await market.listedERC1155(tokenId);
      expect(ListedNFT.owner).to.equal(accounts[0].address);
    });

    it("listed erc721 has correct price", async function () {
      const tokenId = 0;
      await erc1155Token.setApprovalForAll(market.address, true);
      await market.listItemERC1155(tokenId, PRICE, TOKEN_AMOUNT);
      const ListedNFT = await market.listedERC1155(tokenId);
      expect(ListedNFT.price).to.equal(PRICE);
    });

    it("listed erc721 has correct amount", async function () {
      const tokenId = 0;
      await erc1155Token.setApprovalForAll(market.address, true);
      await market.listItemERC1155(tokenId, PRICE, TOKEN_AMOUNT);
      const ListedNFT = await market.listedERC1155(tokenId);
      expect(ListedNFT.amount).to.equal(TOKEN_AMOUNT);
    });

    describe("Reverted", function () {
      it("listedERC721 not listed token", async () => {
        const tokenId = 0;
        await expect(market.listedERC721(tokenId)).to.be.revertedWith(
          "IVKLIMMarket: not listed token"
        );
      });

      it("listedERC1155 not listed token", async () => {
        const tokenId = 0;
        await expect(market.listedERC1155(tokenId)).to.be.revertedWith(
          "IVKLIMMarket: not listed token"
        );
      });

      it("listItemERC1155 token with this id is already in the listing", async () => {
        const tokenId = 0;
        await erc1155Token.setApprovalForAll(market.address, true);
        await market.listItemERC1155(tokenId, PRICE, 1);
        await expect(
          market.listItemERC1155(tokenId, PRICE, 1)
        ).to.be.revertedWith(
          "IVKLIMMarket: token with this id is already in the listing"
        );
      });
    });
  });
});
