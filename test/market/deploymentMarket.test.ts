import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

describe("Deployment market", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();
  });

  it("Should set the right admin role", async function () {
    const adminRole = await market.DEFAULT_ADMIN_ROLE();
    expect(await market.hasRole(adminRole, accounts[0].address)).to.be.true;
  });

  it("Should set right erc721Contract", async function () {
    expect(await market.erc721Contract()).to.equal(erc721Token.address);
  });

  it("Should set right erc1155Contract", async function () {
    expect(await market.erc1155Contract()).to.equal(erc1155Token.address);
  });

  it("Should set right erc20Contract", async function () {
    expect(await market.erc20Contract()).to.equal(erc20Token.address);
  });

  it("Should erc721CollectionLength eq 0", async function () {
    expect(await market.erc721CollectionLength()).to.equal(0);
  });

  it("Should erc1155CollectionLength eq 0", async function () {
    expect(await market.erc1155CollectionLength()).to.equal(0);
  });

  it("supportsInterface return false", async function () {
    expect(await market.supportsInterface("0x75b24222")).to.be.false;
  });
});
