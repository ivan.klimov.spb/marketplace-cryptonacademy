import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;
const TOKEN_ID = 0;
const MINTED_AMOUNT = 1000;

const MINTER_ROLE: string = "MINTER_ROLE";

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

    //mint tokens erc20
    await erc20Token.grantRole(
      ethers.utils.id(MINTER_ROLE),
      accounts[0].address
    );
    await erc20Token.mint(accounts[0].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[1].address, MINTED_AMOUNT);

    //listing
    await erc721Token.approve(market.address, TOKEN_ID);
    await market.listItemERC721(TOKEN_ID, PRICE);

    await erc1155Token.setApprovalForAll(market.address, true);
    await market.listItemERC1155(TOKEN_ID, PRICE, TOKEN_AMOUNT);
  });

  describe("Buy item", function () {
    it("buyItemERC721 tokenERC721 will send to new owner", async function () {
      await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
      await market.connect(accounts[1]).buyItemERC721(TOKEN_ID);
      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(accounts[1].address);
    });

    it("buyItemERC721 salesman will receive price amount", async function () {
      const amountBefore = await erc20Token.balanceOf(accounts[0].address);
      await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
      await market.connect(accounts[1]).buyItemERC721(TOKEN_ID);
      const amountAfter = await erc20Token.balanceOf(accounts[0].address);
      expect(amountAfter.sub(amountBefore).toNumber()).to.equal(PRICE);
    });

    it("buyItemERC1155 tokenERC1155 will send to new owner", async function () {
      await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
      await market.connect(accounts[1]).buyItemERC1155(TOKEN_ID);
      expect(
        await erc1155Token.balanceOf(accounts[1].address, TOKEN_ID)
      ).to.equal(TOKEN_AMOUNT);
    });

    it("buyItemERC1155 salesman will receive price amount", async function () {
      const amountBefore = await erc20Token.balanceOf(accounts[0].address);
      await erc20Token.connect(accounts[1]).approve(market.address, PRICE);
      await market.connect(accounts[1]).buyItemERC1155(TOKEN_ID);
      const amountAfter = await erc20Token.balanceOf(accounts[0].address);
      expect(amountAfter.sub(amountBefore).toNumber()).to.equal(PRICE);
    });

    describe("Reverted", function () {
      it("erc721 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).buyItemERC721(TOKEN_ID + 1)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("erc721 not enough approved tokens", async () => {
        await expect(
          market.connect(accounts[1]).buyItemERC721(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not enough approved tokens");
      });

      it("erc721 not enough amount tokens", async () => {
        await erc20Token.connect(accounts[2]).approve(market.address, PRICE);
        await expect(
          market.connect(accounts[2]).buyItemERC721(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not enough amount tokens");
      });

      it("erc1155 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).buyItemERC1155(TOKEN_ID + 1)
        ).to.be.revertedWith("IVKLIMMarket: current tokenId not listed");
      });

      it("erc1155 current tokenId not listed", async () => {
        await expect(
          market.connect(accounts[1]).buyItemERC1155(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not enough approved tokens");
      });

      it("erc1155 not enough amount tokens", async () => {
        await erc20Token.connect(accounts[2]).approve(market.address, PRICE);
        await expect(
          market.connect(accounts[2]).buyItemERC1155(TOKEN_ID)
        ).to.be.revertedWith("IVKLIMMarket: not enough amount tokens");
      });
    });
  });
});
