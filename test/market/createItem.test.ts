import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;

describe("Create Item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );
  });

  describe("Creat item", function () {
    it("Increase from 0 to 1 erc721CollectionLength after create", async function () {
      expect(await market.erc721CollectionLength()).to.equal(0);
      await market.createItemERC721("", accounts[0].address);
      expect(await market.erc721CollectionLength()).to.equal(1);
    });

    it("Correct owner for new tokenERC721", async function () {
      const tokenId = await market.erc721CollectionLength();
      await market.createItemERC721("", accounts[0].address);

      expect(await erc721Token.ownerOf(tokenId)).to.equal(accounts[0].address);
    });

    it("Correct tokenURI with empty tokenURI and baseURI", async function () {
      const tokenId = await market.erc721CollectionLength();
      await market.createItemERC721("", accounts[0].address);

      expect(await erc721Token.tokenURI(tokenId)).to.equal("");
    });

    it("Correct tokenURI with empty tokenURI and not empty baseURI", async function () {
      const tokenId = await market.erc721CollectionLength();
      await erc721Token.grantRole(
        await erc721Token.MINTER_ROLE(),
        accounts[0].address
      );
      await erc721Token.setBaseURI(URI);
      await market.createItemERC721("", accounts[0].address);

      expect(await erc721Token.tokenURI(tokenId)).to.equal(URI + tokenId);
    });

    it("Correct tokenURI 721", async function () {
      const tokenId = await market.erc721CollectionLength();
      await market.createItemERC721(URI_2, accounts[0].address);

      expect(await erc721Token.tokenURI(tokenId)).to.equal(URI_2 + tokenId);
    });

    it("Increase from 0 to 1 erc1155CollectionLength after create", async function () {
      expect(await market.erc1155CollectionLength()).to.equal(0);
      await market.createItemERC1155("", accounts[0].address, TOKEN_AMOUNT);
      expect(await market.erc1155CollectionLength()).to.equal(1);
    });

    it("Correct owner balance for new tokenERC1155", async function () {
      const tokenId = await market.erc1155CollectionLength();
      await market.createItemERC1155("", accounts[0].address, TOKEN_AMOUNT);

      expect(
        await erc1155Token.balanceOf(accounts[0].address, tokenId)
      ).to.equal(TOKEN_AMOUNT);
    });

    it("Correct tokenURI 1155", async function () {
      const tokenId = await market.erc1155CollectionLength();
      await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

      expect(await erc1155Token.uri(tokenId)).to.equal(URI_2 + "{id}");
    });

    describe("Reverted", function () {
      it("can't be owner with zero address for erc721", async () => {
        await expect(
          market.createItemERC721("", ethers.constants.AddressZero)
        ).to.be.revertedWith("IVKLIMMarket: can't owner with zero address");
      });

      it("can't be owner with zero address for erc1155", async () => {
        await expect(
          market.createItemERC1155(
            "",
            ethers.constants.AddressZero,
            TOKEN_AMOUNT
          )
        ).to.be.revertedWith("IVKLIMMarket: can't owner with zero address");
      });

      it("can't be owner with zero address for erc1155", async () => {
        await expect(
          market.createItemERC1155("", accounts[0].address, 0)
        ).to.be.revertedWith("IVKLIMMarket: amount can't be less than 1");
      });
    });
  });
});
