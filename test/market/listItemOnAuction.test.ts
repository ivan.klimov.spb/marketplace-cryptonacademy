import { expect, assert } from "chai";
import { abi, bytecode } from "../../abi/IVKLIMToken.json";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC721IVKLIM,
  ERC1155IVKLIM,
  IERC20Mintable,
  IVKLIMMarket,
} from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;
let erc1155Token: ERC1155IVKLIM;
let erc20Token: IERC20Mintable;
let market: IVKLIMMarket;

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";

const TOKEN_AMOUNT = 5;
const PRICE = 100;
const TOKEN_ID = 0;
const MINTED_AMOUNT = 1000;

const MINTER_ROLE: string = "MINTER_ROLE";

describe("Listed item", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();

    //ERC721
    const tokenFactoryERC721 = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactoryERC721.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    //ERC1155
    const tokenFactoryERC1155 = await ethers.getContractFactory(
      "ERC1155IVKLIM"
    );
    erc1155Token = <ERC1155IVKLIM>await tokenFactoryERC1155.deploy(URI);
    await erc1155Token.deployed();

    //ERC20
    const tokenFactoryERC20 = await ethers.getContractFactory(abi, bytecode);
    erc20Token = (await tokenFactoryERC20.deploy(
      "IVKLIM",
      "IVK",
      18
    )) as IERC20Mintable;

    //Market
    const marketFactory = await ethers.getContractFactory("IVKLIMMarket");
    market = <IVKLIMMarket>(
      await marketFactory.deploy(
        erc721Token.address,
        erc1155Token.address,
        erc20Token.address
      )
    );
    await market.deployed();

    //give erc721 minter role for market
    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      market.address
    );

    //give erc1155 minter role for market
    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      market.address
    );

    await market.createItemERC721(URI, accounts[0].address);
    await market.createItemERC1155(URI_2, accounts[0].address, TOKEN_AMOUNT);

    //mint tokens erc20
    await erc20Token.grantRole(
      ethers.utils.id(MINTER_ROLE),
      accounts[0].address
    );
    await erc20Token.mint(accounts[0].address, MINTED_AMOUNT);
    await erc20Token.mint(accounts[1].address, MINTED_AMOUNT);

    //approve transfer from account[0] to market
    await erc721Token.approve(market.address, TOKEN_ID);
    await erc1155Token.setApprovalForAll(market.address, true);
  });

  describe("List Item On Auction", function () {
    it("listItemOnAuctionERC721 and after this nft owner is market", async function () {
      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(accounts[0].address);
      await market.listItemOnAuctionERC721(TOKEN_ID, PRICE);
      expect(await erc721Token.ownerOf(TOKEN_ID)).to.equal(market.address);
    });

    it("listItemOnAuctionERC1155 and after this nft owner is market", async function () {
      expect(
        await erc1155Token.balanceOf(accounts[0].address, TOKEN_ID)
      ).to.equal(TOKEN_AMOUNT);
      await market.listItemOnAuctionERC1155(TOKEN_ID, TOKEN_AMOUNT, PRICE);
      expect(await erc1155Token.balanceOf(market.address, TOKEN_ID)).to.equal(
        TOKEN_AMOUNT
      );
    });

    describe("Reverted", function () {
      it("token with this id is already in the listing auction", async () => {
        await await market.listItemOnAuctionERC1155(TOKEN_ID, 1, PRICE);
        await expect(
          market.listItemOnAuctionERC1155(TOKEN_ID, 1, PRICE)
        ).to.be.revertedWith(
          "IVKLIMMarket: token with this id is already in the listing auction"
        );
      });
    });
  });
});
