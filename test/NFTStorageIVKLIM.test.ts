const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { NFTStorageIVKLIM } from "../src/types";

let accounts: SignerWithAddress[];

let nftStorage: NFTStorageIVKLIM;

describe("TokenURI ", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("NFTStorageIVKLIM");
    nftStorage = <NFTStorageIVKLIM>await tokenFactory.deploy();
    await nftStorage.deployed();
  });

  it("baseURI empty by default", async function () {
    expect(await nftStorage.baseUri()).to.equal("");
  });

  it("baseURI eq new value after update", async function () {
    const newBaseUri = "https:/google.com/";
    await nftStorage.setBaseURI(newBaseUri);
    expect(await nftStorage.baseUri()).to.equal(newBaseUri);
  });

  it("tokenURI by default eq baseURI+tokenId", async function () {
    const newBaseUri = "https:/google.com/";
    const tokenId = 1;
    await nftStorage.setBaseURI(newBaseUri);
    expect(await nftStorage.getTokenURI(tokenId)).to.equal(
      newBaseUri + tokenId
    );
  });

  it("tokenURI after update will be eq _tokenURI+tokenId", async function () {
    const newBaseUri = "https:/google.com/";
    const newTokenURI = "https:/apple.com/";
    const tokenId = 1;
    await nftStorage.setBaseURI(newBaseUri);
    await nftStorage.setTokenURI(tokenId, newTokenURI);
    expect(await nftStorage.getTokenURI(tokenId)).to.equal(
      newTokenURI + tokenId
    );
  });
});
