const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC721IVKLIM } from "../../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
const newBaseUri = "https:/google.com/";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;

describe("TokenURI ERC721", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactory.deploy(NAME, SYMBOL);
    await erc721Token.deployed();

    await erc721Token.grantRole(
      await erc721Token.MINTER_ROLE(),
      accounts[0].address
    );
  });

  it("baseURI empty by default", async function () {
    expect(await erc721Token.baseUri()).to.equal("");
  });

  it("baseURI empty by default", async function () {
    expect(await erc721Token.getBaseURI()).to.equal("");
  });

  it("baseURI eq new value after update", async function () {
    await erc721Token.setBaseURI(newBaseUri);
    expect(await erc721Token.baseUri()).to.equal(newBaseUri);
  });

  it("tokenURI mint", async function () {
    const tokenId = 1;
    await erc721Token.mint(
      accounts[0].address,
      tokenId,
      ethers.utils.toUtf8Bytes("")
    );
    const balance = await erc721Token.balanceOf(accounts[0].address);
    expect(balance).to.equal(1);
    expect(await erc721Token.ownerOf(tokenId)).to.equal(accounts[0].address);
  });

  it("tokenURI by default eq baseURI+tokenId", async function () {
    const tokenId = 1;
    await erc721Token.mint(
      accounts[0].address,
      1,
      ethers.utils.toUtf8Bytes("")
    );
    await erc721Token.setBaseURI(newBaseUri);
    expect(await erc721Token.tokenURI(tokenId)).to.equal(newBaseUri + tokenId);
  });

  it("tokenURI after update will be eq _tokenURI+tokenId", async function () {
    const newTokenURI = "https:/apple.com/";
    const tokenId = 1;
    await erc721Token.mint(
      accounts[0].address,
      1,
      ethers.utils.toUtf8Bytes("")
    );
    await erc721Token.setBaseURI(newBaseUri);
    await erc721Token.setTokenURI(tokenId, newTokenURI);
    expect(await erc721Token.tokenURI(tokenId)).to.equal(newTokenURI + tokenId);
  });

  describe("Reverted", function () {
    it("can't set URI query for nonexistent token", async () => {
      const tokenId = 2;
      await expect(
        erc721Token.setTokenURI(tokenId, newBaseUri)
      ).to.be.revertedWith("ERC721Metadata: URI query for nonexistent token");
    });

    it("can't get URI query for nonexistent token", async () => {
      const tokenId = 3;
      await expect(erc721Token.tokenURI(tokenId)).to.be.revertedWith(
        "ERC721Metadata: URI query for nonexistent token"
      );
    });
  });
});
