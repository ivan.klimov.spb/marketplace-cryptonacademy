const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC721IVKLIM } from "../../src/types";

const NAME: string = "IVKLIMNFT721";
const SYMBOL: string = "KLI";
let accounts: SignerWithAddress[];

let erc721Token: ERC721IVKLIM;

describe("Deployment ERC721IVKLIM", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("ERC721IVKLIM");
    erc721Token = <ERC721IVKLIM>await tokenFactory.deploy(NAME, SYMBOL);
    await erc721Token.deployed();
  });

  it("Should set the right admin role", async function () {
    const adminRole = await erc721Token.DEFAULT_ADMIN_ROLE();
    expect(await erc721Token.hasRole(adminRole, accounts[0].address)).to.be
      .true;
  });

  it("Should set right name", async function () {
    expect(await erc721Token.name()).to.equal(NAME);
  });

  it("Should set right symbol", async function () {
    expect(await erc721Token.symbol()).to.equal(SYMBOL);
  });

  it("supportsInterface return false", async function () {
    expect(await erc721Token.supportsInterface("0x75b24222")).to.be.false;
  });
});
