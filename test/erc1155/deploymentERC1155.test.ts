const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC1155IVKLIM } from "../../src/types";

const URI: string = "https://google.com/";
let accounts: SignerWithAddress[];

let erc1155Token: ERC1155IVKLIM;

describe("Deployment ERC1155IVKLIM", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("ERC1155IVKLIM");
    erc1155Token = <ERC1155IVKLIM>await tokenFactory.deploy(URI);
    await erc1155Token.deployed();
  });

  it("Should set the right admin role", async function () {
    const adminRole = await erc1155Token.DEFAULT_ADMIN_ROLE();
    expect(await erc1155Token.hasRole(adminRole, accounts[0].address)).to.be
      .true;
  });

  it("Should set right uri", async function () {
    const tokenId = 1;
    expect(await erc1155Token.uri(tokenId)).to.equal(URI + "{id}");
  });

  it("supportsInterface return false", async function () {
    expect(await erc1155Token.supportsInterface("0x75b24222")).to.be.false;
  });
});
