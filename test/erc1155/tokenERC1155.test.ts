const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC1155IVKLIM } from "../../src/types";

const URI: string = "https://google.com/";
const URI_2: string = "https://apple.com/";
let accounts: SignerWithAddress[];

let erc1155Token: ERC1155IVKLIM;

describe("ERC1155IVKLIM", function () {
  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("ERC1155IVKLIM");
    erc1155Token = <ERC1155IVKLIM>await tokenFactory.deploy(URI);
    await erc1155Token.deployed();

    await erc1155Token.grantRole(
      await erc1155Token.MINTER_ROLE(),
      accounts[0].address
    );
  });

  it("default uri for tokenId is _uri + tokenId", async function () {
    const tokenId = 1;
    expect(await erc1155Token.uri(tokenId)).to.equal(URI + "{id}");
  });

  it(" URI_2 for tokenId is URI_2 + tokenId", async function () {
    const tokenId = 1;
    await erc1155Token.setBaseURI(URI_2);
    expect(await erc1155Token.uri(tokenId)).to.equal(URI_2 + "{id}");
  });

  it("tokenURI by default eq baseURI+tokenId", async function () {
    const tokenId = 1;
    await erc1155Token.setTokenURI(tokenId, URI_2);
    expect(await erc1155Token.uri(tokenId)).to.equal(URI_2 + "{id}");
  });

  it("tokenURI mint", async function () {
    const tokenId = 1;
    const amount = 5;
    await erc1155Token.mint(
      accounts[0].address,
      tokenId,
      amount,
      ethers.utils.toUtf8Bytes("")
    );
    const balance = await erc1155Token.balanceOf(accounts[0].address, tokenId);
    expect(balance).to.equal(amount);
  });

  it("tokenURI mintBatch", async function () {
    const tokenIds = [1, 2];
    const amounts = [5, 3];
    await erc1155Token.mintBatch(
      accounts[0].address,
      tokenIds,
      amounts,
      ethers.utils.toUtf8Bytes("")
    );
    const balance0 = await erc1155Token.balanceOf(
      accounts[0].address,
      tokenIds[0]
    );
    expect(balance0).to.equal(amounts[0]);

    const balance1 = await erc1155Token.balanceOf(
      accounts[0].address,
      tokenIds[1]
    );
    expect(balance1).to.equal(amounts[1]);
  });
});
